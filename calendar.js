var User         = require('../../models/Users/user').User;
var Order        = require('../../models/Orders/Order').Order;
var Campaign     = require('../../models/Orders/Campaign').Campaign;
var Space        = require('../../models/AdministratorPanel/Environments/space').Space;

var async1       = require('async');

var mongoose = require('../../libs/mongoose/config');
let Err   	 = require('../../helpers/Err');
// TODO: remove after specs come to node
let async = require('asyncawait/async');
let await = require('asyncawait/await');

/**
 * @description CalendarController router
 */
class CalendarController {
	constructor() {
	}

	post (req, res) {
		if (!req.body.flag || !req.body.id || req.body.id.length != 24 || !req.body.idActivity || req.body.idActivity.length != 24 ) {
			res.status(400);
			return res.send({});
		}

		async1.waterfall([
			function(callback) {
				Space.findOne({ _id : req.body.id }, function (err, data) {
				   if (err) { return callback(err, 'SpaceFindError'); }
				   if (data) {
					   callback(null, data);
				   } else {
					   callback({}, 'SpaceIdFindError'); // go to last callback
				   }
				});
			},
			function (data, callback) {
				Order.find({ idSpace: data._id }, function (err, order) {
					if (err) { return callback(err, 'OrderFindError'); }
					callback(null, data, order);
				});
			},
			function (data, order, callback) {
				if (req.body.flag == 'getAllOrderInMonth' && !isValidDate(req.body.date)) {
					return callback({}, 'DateError');
				}

				if (order.length == 0) {
					return callback(null, data, order);
				}

				switch (req.body.flag) {
					case 'getAllOrderInMonth':
						var day, year;
						if (req.body.date.indexOf('-') == -1) {
							day = req.body.date.substring(0, req.body.date.indexOf('/'));
							year = req.body.date.substring(0, req.body.date.indexOf('/') + 1);
						} else {
							day = req.body.date.substring(0, req.body.date.indexOf('-'));
							year = req.body.date.substring(0, req.body.date.indexOf('-') + 1);
						}
						order.map(function (item) {
							if (item.dateStart) {
								var dS = new Date(item.dateStart);
								var dE = new Date(item.dateEnd);
								if (dS.getMonth() == day && dS.getFullYear() == year) {
									return item;
								}
								if (dE.getMonth() == day && dE.getFullYear() == year) {
									return item;
								}
							}
							if (item.slotInSpace.length != 0) {
								item.slotInSpace.forEach(function (itemSlot) {
									if (itemSlot.date) {
										var dSpace = new Date(itemSlot.date);
										if (dSpace.getMonth() == day && dSpace.getFullYear() == year) {
											return item;
										}
									}
								})
							}
						});
						callback(null, data, order);
						break;
					case 'saveNewOrder':
						if (data.req_min && data.req_min.toLowerCase().indexOf('no') == -1) {

						} else {
							console.log('slotInSpace');
						}
						break;
					default:
						callback({}, 'FlagOrAcceptedDataError');
				}
			},
			function (data, order, callback) {
				switch (req.body.flag) {
					case 'getAllOrderInMonth':
						if (req.body.date.indexOf('-') == -1) {
							var day, year;
							day  = req.body.date.substring(0, req.body.date.indexOf('/'));
							year = req.body.date.substring(0, req.body.date.indexOf('/') + 1);
						} else {
							day  = req.body.date.substring(0, req.body.date.indexOf('-'));
							year = req.body.date.substring(req.body.date.indexOf('-') + 1);
						}
						var unavailability = [];
						var specialDays    = [];
						var notAvailable   = [{ date:'09', slot: 'slot_1' }, { date:'10', slot: null }, { date:'11', slot: null }];
						var booked         = [{ date:'12', slot: 'slot_1' }, { date:'13', slot: null }, { date:'14', slot: null }];

						data.arrayActivitySettings.forEach(function (item, i, arr) {
							if (item.id == req.body.idActivity) {
								item.unavailability.forEach(function (itemU, i, arr) {
									var arrayTempNotAvailable = [];
									var dF = new Date(itemU.from);
									var dT = new Date(itemU.till);
									if (dF.getMonth() + 1 == day && dF.getFullYear() == year) {
										if (dT.getMonth() + 1 == day && dT.getFullYear() == year) {
											arrayTempNotAvailable = createDateArray(dF, dT);
										} else {
											arrayTempNotAvailable = createDateArray(dF, null);
										}
									} else {
										if (dT.getMonth() + 1 == day && dT.getFullYear() == year) {
											arrayTempNotAvailable = createDateArray(null, dT);
										}
									}
									if (arrayTempNotAvailable.length != 0)
									unavailability.push({message: itemU.message, reason: itemU.reason, date: arrayTempNotAvailable });
								});

								item.specialDates.forEach(function (itemU, i, arr) {
									var arrayTempNotAvailable = [];
									var dF = new Date(itemU.from);
									var dT = new Date(itemU.to);
									if (dF.getMonth() + 1 == day && dF.getFullYear() == year) {
										if (dT.getMonth() + 1 == day && dT.getFullYear() == year) {
											arrayTempNotAvailable = createDateArray(dF, dT);
										} else {
											arrayTempNotAvailable = createDateArray(dF, null);
										}
									} else {
										if (dT.getMonth() + 1 == day && dT.getFullYear() == year) {
											arrayTempNotAvailable = createDateArray(null, dT);
										}
									}
									if (arrayTempNotAvailable.length != 0)
										specialDays.push({price: itemU.price, date: arrayTempNotAvailable });
								});
								return false;
							}
						});


						callback(null, '',  {
							notAvailable   : notAvailable,
							unavailability : unavailability,
							specialDays    : specialDays,
							booked         : booked
						});
						break;
					case 'saveNewOrder':
						break;
					default:
						callback({}, 'FlagOrAcceptedDataError');
				}
			}
		], function (err, status, data) {
			if (err) {
				console.log(status);
				switch (status) {
					case 'NoError':
						res.status(200);
						return res.send({});
					case 'SendData':
						res.status(200);
						return res.send(data);
					case 'BadDataStart':
						res.status(400);
						return res.send({ 'error': 'BadDataStart', 'description': 'bad data start! This Space contains minimum requirements' });
					case 'BadDataEnd':
						res.status(400);
						return res.send({ 'error': 'BadEnd', 'description': 'bad data end! This Space contains minimum requirements. \"data end\" is necessarily' });
					case 'FlagOrAcceptedDataError':
						res.status(400);
						return res.send({});
					case 'DateError':
						res.status(400);
						return res.send({});
					case 'SpaceIdFindError':
						res.status(400);
						return res.send({});
					default:
						res.status(500);
						return res.send({});
				}
			}
			res.status(200);
			return res.send(data);
		});
	}

	/**
	 * saves booking to Campaign and Order models
	 * @param  {object} req
	 * @param  {object} res
	 * @return {}
	 */
	getBookings (req, res) {
		return async (function(req, res) {
			let booking = {
				_creator: req.session.user.company_id,
			};
			let populate = false;
			let fields;
			if (req.query.fields) {
				fields = req.query.fields
				.split(",")
				.map((v)=>{populate = (v=='orders')?true:false; return v.trim()})
				.join(" ");
			}

			fields = fields || '_id status name created _creator _creatorCompany';
			try {
				if (populate) {
					booking = await (
						Campaign
							.find(booking)
							.select(fields)
							.populate({
								path:'_creator',
								select: 'name'
							})
							.populate({
								path:'_creatorCompany',
								select: 'name'
							})
							.populate({
								path:'orders',
								populate: {
									path: 'city location section space environment category',
									select: 'name subtitle nameLocation' }
							})
							.exec());
				} else {
					booking = await (
						Campaign
							.find(booking)
							// .populate('_creator')
							.select(fields)
							.exec());
				}
			} catch(err) {
				return res.status(400).json(err.message);
			}

			// booking = {};
			return res.status(200).json(booking);

		})(req, res);
	}

	/**
	 * saves booking to Campaign and Order models
	 * @param  {object} req
	 * @param  {object} res
	 * @return {}
	 */
	saveBooking (req, res) {
		return async (function(req, res) {
			let booking = {
				_creator: req.session.user._id,
				_creatorCompany: req.session.user.company_id,
				name: req.body.name,
				orders: req.body.orders
			};

			try {
				booking = await (CalendarController._saveBooking(booking));
			} catch(err) {
				return res.status(400).json(err.message);
			}
			// delete console
			console.log(booking);
			// booking = {};
			return res.status(200).json({_id:booking._id});

		})(req, res);
	}
	/**
	 * updates Campaign and performs checks on conflicts
	 * @param  {object} req
	 * @param  {object} res
	 * @return {}
	 */
	finishBooking(req, res) {
		return async (function(req, res) {
			let conflictingBooking = false;
			let booking = {
				_id: req.body._id
				// _creator: req.session.user.company_id,
				// name: req.body.name,
				// orders: req.body.orders
			};

			// get campaign
			try {
				booking = await (
					Campaign
					.findOne(booking)
					.populate('orders')
					.exec());
			} catch(err) {
				err = new Err(new Error('No such campaign'), 'No such campaign', 400);
				return res.status(err.status).json(err.message);
			}
			// check booking for conflicts
			try {
				conflictingBooking = await (CalendarController._getConflictingBooking(booking));
			} catch(err) {
				return res.status(err.status).json(err.message);
			}

			if (conflictingBooking !== null) {
				// // what to do if there is a conflict
				// if (booking.status != DRAFT) {
				// 	new Err(new Error(), 'cannot save until reserved slots are edited', 400)
				// 	return res.status(err.status).json(err.message);
				// }
				// if (booking.status != DRAFT_WARNING) {
				// 	send wanring 48 to conflictingBooking
				// }
				// conflictingBooking.status = DRAFT_WARNING
				// update conflictingBooking
				// set booking.status = WAITING_FOR_CONFIRMATION
				// TODO: repear status
				conflictingBooking.status = Campaign.statuses().DRAFT_WARNING;
				booking.status = Campaign.statuses().TO_BE_ADJUSTED;

				try {
					conflictingBooking = await (conflictingBooking.save())
				} catch(err) {
					err = new Err(new Error('Cannot update conflicting booking'), 'Cannot update conflicting booking', 400);
				 	return res.status(err.status).json(err.message);
				}
			}

			booking.status = Campaign.statuses().WAITING_FOR_CONFIRMATION;

			// TODO: delete console
			console.log("ConfBook: ", conflictingBooking);

			// if no conflicts - save
			// save booking to DB
			try {
				booking = await (booking.save())
			} catch(err) {
				err = new Err(new Error('Cannot update booking'), 'Cannot update booking', 400);
			 	return res.status(err.status).json(err.message);
			}

			booking = {};
			return res.status(200).json(booking);

		})(req, res);
	}

	/**
	 * delete order by ID
	 * @param  {object} req
	 * @param  {object} res
	 * @return {}
	 */
	deleteOrderById(req, res) {
		return async (function(req, res) {
			let deleteOrder;
			try {
				deleteOrder = await (Order.remove({ _id: req.params._id }));
			} catch(err) {
				return res.status(500).json(err.message);
			}

			_findAndDeleteCampaignWithThisOrder(req.params._id);

			return res.status(200).json({});
		})(req, res);
	}

	/**
	 * delete campaign by ID
	 * @param {object} req
	 * @param {object} res
	 * @return {}
	 */
	deleteCampaignById(req, res) {
		return async (function(req, res) {
			let deleteCampaign;
			try {
				deleteCampaign = await (Campaign.remove({ _id: req.params._id }));
			} catch(err) {
				return res.status(500).json(err.message);
			}

			try {
				await (Order.remove({ _campaign: req.params._id }));
			} catch(err) {
				return res.status(500).json(err.message);
			}

			return res.status(200).json({});
		})(req, res);
	}

	/**
	 * delete campaign by ID
	 * @param {object} req
	 * @param {object} res
	 * @return {}
	 */
	deleteSlotInOrderById(req, res) {
		return async (function(req, res) {
			let order;
			try {
				order = await (Order.findOne({ _id: req.params._id }).exec());
			} catch(err) {
				return res.status(500).json(err.message);
			}

			for (var i = 0; i < order.slots.length; ++i) {
				if (order.slots[i].num == req.params.num) {
					order.slots.splice(i, 1);
					break;
				} 
			}

			try {
				await (order.save());
			} catch(err) {
				return res.status(500).json(err.message);
			}
			

			return res.status(200).json(order);
		})(req, res);
	}

	/**
	 * check if there is a conflict between new booking and existing
	 * @param  {object}  booking {_creator,name,orders}
	 * @return {object || null}         conflictingBooking or null
	 */
	static _getConflictingBooking (booking) {
		return async (function(booking) {
			// check for conflicts
			let foundBookings;
			let currentDate = new Date();
			let month = currentDate.getMonth();
			let year = currentDate.getFullYear();
			let day = currentDate.getDate();

			let spaces = [];
			let dates  = [];

			for (var i = booking.orders.length - 1; i >= 0; i--) {
				spaces.push(booking.orders[i].space);
				dates.push(booking.orders[i].date); //тут изменить формат
			}

			// maybe replace by Order find
			try {
				foundBookings = await (
					Campaign
						.find({ status: Campaign.statuses().DRAFT_PRIOR }) //change to proper status with enum
						.populate({
							path: 'orders',
							select: 'space slots date',
							match: {
								// activity : { $in: activities },
								// city : { $in: cities },
								// environment : { $in: environments },
								// category : { $in: categories },
								// location : { $in: locations },
								space : { $in: spaces },
								options: {sort: {date: -1}}
								// date: { $in: dates } //и тут изменить формат
							}
						})
						.exec()
				);
			} catch(err) {
				throw new Err(err, 'Cannot find bookings', 500);
			}
			// TODO: жесть!!!
			// for (var i = foundBookings.length - 1; i >= 0; i--) {
			// 	for (var j = foundBookings[i].orders.length - 1; j >= 0; j--) {

			// 		for (var k = foundBookings[i].orders[j].slots.length - 1; k >= 0; k--) {
			// 			for (var l = booking.orders.length - 1; l >= 0; l--) {
			// 				for (var m = booking.orders[l].slots.length - 1; m >= 0; m--) {
			// 					if (foundBookings[i].orders[j].slots[k].start == booking.orders[l].slots[m].start
			// 						&& foundBookings[i].orders[j].slots[k].end == booking.orders[l].slots[m].end) {
			// 							return foundBookings[i];
			// 					}
			// 				}
			// 			}

			// 		}
			// 	}
			// }

			// TODO: preLast version
			return findInArray(foundBookings, booking);


			
		})(booking);
	}

	/**
	 * creates new booking and saves it to DB
	 * @param  {object} booking {_creator,name,orders}
	 * @return {object}         saved Campaign object || {}
	 */
	static _saveBooking (booking) {
		return async (function(booking) {
			var newCampaign;
			let savedOrders;

			let incommingCampaign = new Campaign({
				_creator:  booking._creator,
				_creatorCompany: booking.company_id,
				status: booking.status || Campaign.statuses().DRAFT_PRIOR,
				name: booking.name
			});

			try {
				newCampaign = await (incommingCampaign.save());
			} catch(err) {
				// TODO: edit err.codes later1
				throw new Err(err, 'Cannot save campaign', 500);
			}

			let currentDate = new Date(2016, 11, 11);

			for (var i = booking.orders.length - 1; i >= 0; i--) {
				booking.orders[i] = new Order({
					_campaign	:  newCampaign._id,
					space 		:  booking.orders[i].space,
					location 	:  booking.orders[i].location,
					category 	:  booking.orders[i].category,
					environment :  booking.orders[i].environment,
					city 		:  booking.orders[i].city,
					section 	:  booking.orders[i].section,
					date 		: currentDate,
					//status: '-1', // check
					//options: [-1], //check, Ramzan says this is only for later
					//comment: '-1', // check
					slots: booking.orders[i].slots, // check if needs to be a schema object
					created: currentDate
				})
				//booking.orders[i].save();
				for (var j = booking.orders[i].slots.length - 1; j >= 0; j--) {
					incommingCampaign.totalPrice += parseFloat(booking.orders[i].slots[j].price);
				}

				incommingCampaign.orders.push(booking.orders[i]);
			}
			// save all incomming data
			// TODO: check data, check conflicts and TTL mongoose
			try {
				savedOrders = await (Order.insertMany(booking.orders));
			} catch(err) {
				throw new Err(err, 'Cannot save orders', 500);
			}
			// TODO: handle all error method via try catch
			incommingCampaign.save();
			// incommingCampaign = {};
			return incommingCampaign;

		})(booking);
	}

	/**
	 * find orders in BD
	 * @param  {object} _id
	 * @return {object} _id for delete
	 */
	static _findAndDeleteCampaignWithThisOrder(_id) {
		return async (function(_id) {
			let campaign = [];
			let idForDelete = [];
			try {
				await (
					Campaign
						.find({ orders: _id })
						.forEach(function (doc) {
							for (let j = 0; j < doc.orders.length; ++j) {
								if (doc.orders[j].toString() == _id) {
									doc.orders.splice(j, 1);
									if (doc.orders.length === 0) {
										idForDelete.push({_id : doc._id});
									}
									break;
								}
							}
							doc.save();
						})
						.exec()
					);
			} catch(err) {
				return res.status(500).json(err.message);
			}
			
			if (idForDelete.length != 0) {
				try {
					await (Campaign.remove({ $or: idForDelete }));
				} catch(err) {
					return res.status(500).json(err.message);
				}
			}

		})
	}
}

function isValidDate (date) {
    var matches = /^(\d{1,2})[-\/](\d{4})$/.exec(date);
    if (matches == null) return false;
    var m = matches[1] - 1, y = matches[2];
    var composedDate = new Date(y, m);
    return composedDate.getMonth() == m &&
           composedDate.getFullYear() == y;
}

function isValidDateFull (date) {
    var matches = /^(\d{1,2})[-\/](\d{1,2})[-\/](\d{4})$/.exec(date);
    if (matches == null) return false;
    var d = matches[1] - 1, m = matches[2], y = matches[3];
    var composedDate = new Date(y, m, d);
    return composedDate.getDate() == d &&
           composedDate.getMonth() == m &&
           composedDate.getFullYear() == y;
}

function createDateArray(from, till) {
    var array = [];

    if (from && till && till.getDate() - from.getDate() > 0) {
       for (var i = 0; i < till.getDate() - from.getDate() + 1; ++i) {
           array.push(from.getDate() + 1 + i);
       }
       return array;
    }

    if (from && !till) {
        var lastDay = new Date(from.getFullYear(), from.getMonth() + 1, 0).getDate();
        for (var i = 0; i < lastDay - from.getDate(); ++i) {
            array.push(from.getDate() + 1 + i);
        }
        return array;
    }

    if (!from && till) {
        for (var i = 1; i < till.getDate() + 2; ++i) {
            array.push(i);
        }
        return array;
    }
    return [];
}


function findInArray(foundBookings, booking) {
	if (foundBookings.length == 0 || booking.orders.length == 0) { return null; }
	let i = 0;
	let j = foundBookings[i].orders.length - 1;
	let l = booking.orders.length - 1;

	for (; j >= 0; --j) {
		let k = 0; 
		for (; l >= 0; --l) {
			for (let m = booking.orders[l].slots.length - 1; m >= 0; --m) {
				if (foundBookings[i].orders[j].slots[k].start == booking.orders[l].slots[m].start
					&& foundBookings[i].orders[j].slots[k].end == booking.orders[l].slots[m].end) {
						return foundBookings[i];
				}
			}
			if (l === 0 && k < foundBookings[i].orders[j].slots.length) {
				++k;
				if (foundBookings[i].orders[j].slots[k] == undefined) {
					k = 0;
					break;
				}
			}
		}
		if (j === 0 && i < foundBookings.length) {
			++i;
			j = foundBookings[i].orders.length - 1;
		}
	}
	return null;
}


module.exports = exports = CalendarController;