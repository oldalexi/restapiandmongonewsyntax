var User    = require('../../../models/Users/user').User;
var Company = require('../../../models/Users/company').Company;

exports.post = function(req, res) {
    if (req.body.vat) {
        Company.findOne({ vat: req.body.vat }, function(err, data) {
            if (data) {
                res.send(data);
            } else {
                res.send({});
            }
        })
    } else {
        res.status(400);
        res.send('Bad vat');
    }
};