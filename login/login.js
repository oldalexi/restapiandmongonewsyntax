var User    = require('../../../models/Users/user').User;
var Company = require('../../../models/Users/company').Company;
var async   = require('async');

exports.post = function(req, res) {
    if (req.body.flag == 'getAllUsers') {
        User.find({}, function (err, data) {
            if (err) {
                res.status(500);
                res.send(err);
            } else {
                res.send(data);
            }
        })
    } else {
        if (req.body.email == '') {
            res.send('not enter email');
        }
        if (req.body.password == '') {
            res.send('not enter password');
        }
        async.waterfall([
            function(callback) {
                User.findOne({ email: req.body.email }, callback);
            },
            function(user, callback) {
                if (user) {
                    if (user.checkPassword(req.body.password)) {
                        if (user.company) {
                            Company.findOne({_id: user.company}, function (err, data) {
                                if (err) {
                                    callback(err, {});
                                } else {
                                    if (data) {
                                        var newData = {
                                            id            : user._id,
                                            booking       : data.booking,
                                            venues        : data.venues,
                                            reach         : data.reach,
                                            username      : user.username,
                                            active        : user.active,
                                            company_id    : data._id,
                                            company_name  : data.name,
                                            company_email : data.email
                                        };
                                        callback(null, newData);
                                    } else {
                                        callback(null, {});
                                    }
                                }
                            });
                        } else {
                            callback(null, {});
                        }
                    } else {
                        res.status(403);
                        res.end('Bad password');
                    }
                } else {
                    res.status(403);
                    res.end('Not user');
                }
            }
        ], function(err, user) {
            if (err) return next(err);
            if (user.active) {
                req.session.user = {
                	_id : user.id,
                	company_id : user.company_id
                };
                res.send(user);
            } else {
                req.status(401);
                res.send('not OK');
            }

        });
    }
};