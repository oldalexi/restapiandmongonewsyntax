var User    = require('../../../models/Users/user').User;
var Company = require('../../../models/Users/company').Company;
var async = require('async');

exports.post = function(req, res) {
    if (req.body.email == '') {
        res.end('not enter email');
    }
    if (req.body.password == '') {
        res.end('not enter password');
    }
    if (req.body.vat == '') {
        res.end('not enter VAT');
    }
    async.waterfall([
        function(callback) {
            User.findOne({ email: req.body.email }, callback);
        },
        function(user, callback) {
            if (user) {
                res.status(403);
                res.end('email was already taken');
                callback(null, 'Bad');
            } else {
                User.findOne({ username: req.body.username }, callback);
            }
        },
        function(user, callback) {
            if (user) {
                res.status(403);
                res.end('username was already taken');
            } else {
                callback(null, 'OK');
            }
        }
    ], function(err, status) {
        if (err) return next(err);
        if (status == 'OK') {
            Company.findOne({ vat: req.body.vat }, function (err, data) {
                if (err) {
                    res.status(500);
                    res.send(err);
                } else {
                    if (data) {
                        var hash ='';
                        while(hash.length < 64)
                            hash += String.fromCharCode(Math.random() *127).replace(/\W|\d|_/g,'');
                        new User({
                            username   : req.body.username,
                            email      : req.body.email,
                            password   : req.body.password,
                            active     : true, // for test
                            valid_hash : hash, // for email
                            company    : data._id
                        }).save(function (err, user) {
                            if (err) {
                                res.status(500);
                                res.send('Error in server!');
                            } else {
                                res.send('OK');
                            }
                        });
                    } else {
                        console.log(req.body.name_c)
                        console.log(req.body.email_c)
                        console.log(req.body)
                        new Company({
                            name        : (req.body.name_c) ? req.body.name_c : '',
                            vat         : req.body.vat,
                            street      : (req.body.street) ? req.body.street : '',
                            postal_code : (req.body.postal_code) ? req.body.postal_code : '',
                            city        : (req.body.city) ? req.body.city : '',
                            country     : (req.body.country) ? req.body.country : '',
                            email       : (req.body.email_c) ? req.body.email_c : '',
                            phone       : (req.body.phone) ? req.body.phone : '',
                            booking     : 0,
                            venues      : 0,
                            reach       : 0
                        }).save(function (err, data) {
                            if (err) {
                                res.status(500);
                                res.send('Error in server!');
                            } else {
                                    var hash ='';
                                    while(hash.length < 64)
                                        hash += String.fromCharCode(Math.random() *127).replace(/\W|\d|_/g,'');
                                new User({
                                    username   : req.body.username,
                                    email      : req.body.email,
                                    password   : req.body.password,
                                    active     : true, // for test
                                    valid_hash : hash, // for email
                                    company    : data._id
                                }).save(function (err, user) {
                                    if (err) {
                                        res.status(500);
                                        res.send('Error in server!');
                                    } else {
                                        res.send('OK');
                                    }
                                });
                            }
                        });
                    }
                }
            })

        }
    });
};