ExterionLive.controller('LoginCtrl', ['$scope', '$location', 'ModalService', 'AuthService', 'Globals', 'Api', 'close', function ($scope, $location, ModalService, AuthService, Globals, Api, close) {
	
	$scope.globals = Globals;
	$scope.loginForm = {};
	$scope.registerForm = {step1: {}, step2: {}};
	$scope.config = {
		loginVisible: true, 
		registerVisible: false, 
		registerStep: 1, 
		usernamePattern: '[a-zA-Z0-9]*',
		passwordPattern: /^(?=.*\d)(?=.*[a-z]).{5,18}$/,
	};
	$scope.close = close;

	$scope.showRegister = function() {
		$scope.config.loginVisible = false;
		$scope.config.registerVisible = true;
	};
	$scope.showLogin = function() {
		$scope.config.loginVisible = true;
		$scope.config.registerVisible = false;
	};
	$scope.toRegisterStep2 = function(valid) {
		if(valid) {
			$scope.config.registerStep = 2;
		}
	};
	$scope.toRegisterStep1 = function() {
		$scope.config.registerStep = 1;
	};
	$scope.register = function(valid1, valid2) {
		if($scope.config.agree && valid1 && valid2) Api.users.signup(angular.merge($scope.registerForm.step1, $scope.registerForm.step2)).then(function(res) {
			$scope.config.registerStep = 3;
		}, function(error) {
			alert(error.data);
		});
	};
	$scope.login = function(valid) {
		if(valid) Api.users.login($scope.loginForm).then(function(res) {
			AuthService.saveUser(res.data);
			close(res.data);
		}, function(error) {
			alert(error.data);
		});
	};

	var vatThrottled = _.debounce(searchVat, 500);
	function searchVat(vat) {
		Api.users.vat(vat).then(function(res) {
			var data = res.data;
			$scope.registerForm.step2.name_c = data.name;
			$scope.registerForm.step2.city = data.city;
			$scope.registerForm.step2.country = data.country;
			$scope.registerForm.step2.email_c = data.email;
			$scope.registerForm.step2.phone = data.phone;
			$scope.registerForm.step2.postal_code = data.postal_code;
			$scope.registerForm.step2.street = data.street;
			$scope.registerForm.step2.vat = data.vat;
		}, function(err) {
		});
	}
	$scope.$watch('registerForm.step2.vat', function(newValue, oldValue) {
		if(newValue && newValue != oldValue) vatThrottled(newValue);
	});

}]);