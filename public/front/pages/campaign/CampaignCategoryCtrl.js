ExterionLive.controller('CampaignCategoryCtrl', ['$scope', '$stateParams', '$state', 'ModalService', 'AuthService', 'Globals', 'categories', function ($scope, $stateParams, $state, ModalService, AuthService, Globals, categories) {
	
	$scope.globals = Globals;
	$scope.Math = window.Math;
	if(!$stateParams.data) history.back();
	$scope.data = JSON.parse(b64_to_utf8($stateParams.data || '') || '{}');
	$scope.categories = categories.data;

	$scope.next = function(category) {
		$scope.data.category = {id: category._id, name: category.name};
		$state.go('campaign_location', {data: utf8_to_b64(JSON.stringify($scope.data))});
	};

}]);