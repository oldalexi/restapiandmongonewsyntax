ExterionLive.controller('CampaignSpaceCtrl', ['$scope', '$stateParams', '$state', 'ModalService', 'AuthService', 'Globals', 'Api', 'spaces', function ($scope, $stateParams, $state, ModalService, AuthService, Globals, Api, spaces) {
	
	$scope.globals = Globals;
	if(!$stateParams.data) history.back();
	$scope.data = JSON.parse(b64_to_utf8($stateParams.data || '') || '{}');
	$scope.spaces = spaces.data;
	$scope.b_items = [];
	$scope.search = {val: ''};

	$scope.next = function(space) {
		$scope.data.space = {id: space._id, name: space.name};
		$state.go('campaign_planner', {data: utf8_to_b64(JSON.stringify($scope.data))});
	};

	$scope.search = function() {
		var val = $scope.search.val.trim();
		if(!val) {
			$scope.spaces = angular.extend([], $scope.b_items);
			$scope.b_items = [];
			return;
		} 
		if(val && !$scope.b_items.length) $scope.b_items = angular.extend([], $scope.spaces);
		Api.campaign.space($scope.data.location.id, $scope.data.section.id, val).then(function(res) {
			if(res.data.length) $scope.spaces = res.data;
		});
	};

}]);