ExterionLive.controller('CampaignCityCtrl', ['$scope', '$state', '$stateParams', 'ModalService', 'AuthService', 'Globals', 'Api', 'cities', function ($scope, $state, $stateParams, ModalService, AuthService, Globals, Api, cities) {
	
	$scope.globals = Globals;
	if(!$stateParams.data) history.back();
	$scope.data = JSON.parse(b64_to_utf8($stateParams.data || '') || '{}');
	$scope.cities = cities.data;
	$scope.b_items = [];
	$scope.search = {val: ''};
	
	$scope.next = function(city) {
		$scope.data.city = {id: city._id, name: city.name};
		$state.go('campaign_env', {data: utf8_to_b64(JSON.stringify($scope.data))});
	};

	$scope.search = function() {
		var val = $scope.search.val.trim();
		if(!val) {
			$scope.cities = angular.extend([], $scope.b_items);
			$scope.b_items = [];
			return;
		} 
		if(val && !$scope.b_items.length) $scope.b_items = angular.extend([], $scope.cities);
		Api.campaign.city($scope.data.section.id, val).then(function(res) {
			if(res.data.length) $scope.cities = res.data;
		});
	};

}]);