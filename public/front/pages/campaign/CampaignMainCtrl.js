ExterionLive.controller('CampaignMainCtrl', ['$scope', '$location', 'ModalService', 'AuthService', 'Globals', function ($scope, $location, ModalService, AuthService, Globals) {
	
	$scope.globals = Globals;

	$scope.modal = function(data, afterSave, afterClose) {
        var c_data = angular.merge({}, data);
        ModalService.showModal({
            templateUrl: '/pages/campaign/create_modal.html',
            controller: 'CampaignCreateModalCtrl',
            inputs: {
                data: c_data,
                afterSave: function(res) {
                    if(afterSave) afterSave(res, data);
                }
            }
        }).then(function (modal) {
            Globals.modalOpen = true;
            modal.close.then(function (result) {
                Globals.modalOpen = false;
                if(afterClose) afterClose(result);
            });
        });
    };

}]);