ExterionLive.controller('CampaignLocationCtrl', ['$scope', '$stateParams', '$state', 'ModalService', 'AuthService', 'Globals', 'Api', 'locations', function ($scope, $stateParams, $state, ModalService, AuthService, Globals, Api, locations) {
	
	$scope.globals = Globals;
	if(!$stateParams.data) history.back();
	$scope.data = JSON.parse(b64_to_utf8($stateParams.data || '') || '{}');
	$scope.locations = locations.data;
	$scope.b_items = [];
	$scope.search = {val: ''};

	$scope.next = function(location) {
		$scope.data.location = {id: location.id, name: location.nameLocation};
		$state.go('campaign_space', {data: utf8_to_b64(JSON.stringify($scope.data))});
	};

	$scope.search = function() {
		var val = $scope.search.val.trim();
		if(!val) {
			$scope.locations = angular.extend([], $scope.b_items);
			$scope.b_items = [];
			return;
		} 
		if(val && !$scope.b_items.length) $scope.b_items = angular.extend([], $scope.locations);
		Api.campaign.location($scope.data.city.id, $scope.data.env.id, $scope.data.category.id, val).then(function(res) {
			if(res.data.length) $scope.locations = res.data;
		});
	};

}]);