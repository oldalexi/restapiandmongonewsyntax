ExterionLive.controller('CampaignCreateModalCtrl', ['$scope', '$state', 'ModalService', 'AuthService', 'Globals', 'close', function ($scope, $state, ModalService, AuthService, Globals, close) {
	
	$scope.globals = Globals;
	$scope.data = {name: ''};

	$scope.next = function() {
		if($scope.data.name) {
			$state.go('campaign_section', {data: utf8_to_b64(JSON.stringify($scope.data))});
			close();
		}
	};

	$scope.planner = function() {
		if($scope.data.name) {
			$state.go('campaign_planner', {data: utf8_to_b64(JSON.stringify($scope.data))});
			close();
		}
	};

	$scope.close = close;

}]);