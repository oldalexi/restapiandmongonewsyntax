ExterionLive.controller('CampaignEnvCtrl', ['$scope', '$state', '$stateParams', 'ModalService', 'AuthService', 'Globals', 'envs', function ($scope, $state, $stateParams, ModalService, AuthService, Globals, envs) {
	
	$scope.globals = Globals;
	$scope.Math = window.Math;
	if(!$stateParams.data) history.back();
	$scope.data = JSON.parse(b64_to_utf8($stateParams.data || '') || '{}');
	$scope.envs = envs.data;

	$scope.next = function(env) {
		$scope.data.env = {id: env._id, name: env.name};
		$state.go('campaign_category', {data: utf8_to_b64(JSON.stringify($scope.data))});
	};

}]);