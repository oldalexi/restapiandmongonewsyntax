ExterionLive.controller('CampaignSectionCtrl', ['$scope', '$state', '$stateParams', 'ModalService', 'AuthService', 'Globals', 'Api', 'sections', function ($scope, $state, $stateParams, ModalService, AuthService, Globals, Api, sections) {
	
	$scope.globals = Globals;
	$scope.sections = sections.data;
	$scope.b_sections = [];
	$scope.search = {val: ''};
	
	if(!$stateParams.data) history.back();
	$scope.data = JSON.parse(b64_to_utf8($stateParams.data || '') || '{}');

	$scope.next = function(section) {
		$scope.data.section = {id: section._id};
		$state.go('campaign_city', {data: utf8_to_b64(JSON.stringify($scope.data))});
	};

	$scope.search = function() {
		var val = $scope.search.val.trim();
		if(!val) {
			$scope.sections = angular.extend([], $scope.b_sections);
			$scope.b_sections = [];
			return;
		} 
		if(val && !$scope.b_sections.length) $scope.b_sections = angular.extend([], $scope.sections);
		Api.campaign.section(val).then(function(res) {
			if(res.data.length) $scope.sections = res.data;
		});
	};

}]);