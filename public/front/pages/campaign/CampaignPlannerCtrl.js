ExterionLive.controller('CampaignPlannerCtrl', ['$scope', '$stateParams', '$state', 'ModalService', 'AuthService', 'Globals', 'Api', 'sections', 'cities', 'envs', 'categories', 'locations', 'spaces', 'calendar', function ($scope, $stateParams, $state, ModalService, AuthService, Globals, Api, sections, cities, envs, categories, locations, spaces, calendar) {
	
	$scope.Math = window.Math;
	$scope.globals = Globals;

	$scope.data = JSON.parse(b64_to_utf8($stateParams.data || '') || '{}');
	$scope.sections = sections.data;
	$scope.cities = cities.data;
	$scope.envs = envs.data;
	$scope.categories = categories.data;
	$scope.locations = locations.data;
	$scope.spaces = spaces.data;
	$scope.calendarResolved = false;
	$scope.orders = {};
	$scope.sums = {};

	var months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

	$scope.calendar = [];

	//debugger

	$scope.data.section = $scope.data.section || {};
	$scope.data.city = $scope.data.city || {};
	$scope.data.env = $scope.data.env || {};
	$scope.data.category = $scope.data.category || {};
	$scope.data.location = $scope.data.location || {};
	$scope.data.space = $scope.data.space || {};

	$scope.getCalendar = function(num, date, noResolve) {
		if(!noResolve) $scope.calendarResolved = false;
		Api.campaign.calendar($scope.data.space.id, $scope.data.section.id, date.getMonth(), date.getFullYear()).then(function(res) {
			$scope.calendar[num].data = $scope.parseData(res.data, date, num);
			$scope.calendarResolved = true;
		});
	};

	$scope.getCalendars = function() {
		var currentDate = new Date();
		var currentMonth = currentDate.getMonth();
		var currentYear = currentDate.getFullYear();
		var nextDate = new Date(currentMonth == 11 ? currentYear + 1 : currentYear, currentMonth == 11 ? 0 : currentMonth + 1, 1);
		$scope.calendar[0] = calcCalendar(currentDate);
		$scope.calendar[1] = calcCalendar(nextDate);
		if(calendar.data) {
			$scope.calendar[0].data = $scope.parseData(calendar.data, $scope.calendar[0].date, 0);
			calendar.data = null;
		} else $scope.getCalendar(0, currentDate);
		$scope.getCalendar(1, nextDate, true);
	};

	$scope.$watch('data.section.id', function(newValue, oldValue) {
		if(newValue) {
			for(var k in $scope.sections) {
				if($scope.sections[k]._id == newValue) {
					$scope.data.section = angular.extend({}, $scope.sections[k]);
					$scope.data.section.id = $scope.data.section._id;
					break;
				}
			}
		}
		if(!newValue || newValue == oldValue) return;
		$scope.data.city.id = 0;
		$scope.data.env.id = 0;
		$scope.data.category.id = 0;
		$scope.data.location.id = 0;
		$scope.data.space.id = 0;
		
		$scope.cities = [];
		$scope.envs = [];
		$scope.categories = [];
		$scope.locations = [];
		$scope.spaces = [];
		
		Api.campaign.city($scope.data.section.id).then(function(res) {
			$scope.cities = res.data;
		});
	});

	$scope.$watch('data.city.id', function(newValue, oldValue) {
		if(newValue) {
			for(var k in $scope.cities) {
				if($scope.cities[k]._id == newValue) {
					$scope.data.city = angular.extend({}, $scope.cities[k]);
					$scope.data.city.id = $scope.data.city._id;
					break;
				}
			}
		}
		if(!newValue || newValue == oldValue) return;
		$scope.data.env.id = 0;
		$scope.data.category.id = 0;
		$scope.data.location.id = 0;
		$scope.data.space.id = 0;

		$scope.envs = [];
		$scope.categories = [];
		$scope.locations = [];
		$scope.spaces = [];
		
		Api.campaign.env($scope.data.city.id).then(function(res) {
			$scope.envs = res.data;
		});
	});

	$scope.$watch('data.env.id', function(newValue, oldValue) {
		if(newValue) {
			for(var k in $scope.envs) {
				if($scope.envs[k]._id == newValue) {
					$scope.data.env = angular.extend({}, $scope.envs[k]);
					$scope.data.env.id = $scope.data.env._id;
					break;
				}
			}
		}
		if(!newValue || newValue == oldValue) return;
		$scope.data.category.id = 0;
		$scope.data.location.id = 0;
		$scope.data.space.id = 0;

		$scope.categories = [];
		$scope.locations = [];
		$scope.spaces = [];
		
		Api.campaign.category($scope.data.city.id, $scope.data.env.id).then(function(res) {
			$scope.categories = res.data;
		});
	});

	$scope.$watch('data.category.id', function(newValue, oldValue) {
		if(newValue) {
			for(var k in $scope.categories) {
				if($scope.categories[k]._id == newValue) {
					$scope.data.category = angular.extend({}, $scope.categories[k]);
					$scope.data.category.id = $scope.data.category._id;
					break;
				}
			}
		}
		if(!newValue || newValue == oldValue) return;
		$scope.data.location.id = 0;
		$scope.data.space.id = 0;

		$scope.locations = [];
		$scope.spaces = [];
		
		Api.campaign.location($scope.data.city.id, $scope.data.env.id, $scope.data.category.id).then(function(res) {
			$scope.locations = res.data;
		});
	});

	$scope.$watch('data.location.id', function(newValue, oldValue) {
		if(newValue) {
			for(var k in $scope.locations) {
				if($scope.locations[k].id == newValue) {
					$scope.data.location = angular.extend({}, $scope.locations[k]);
					$scope.data.location.name = $scope.data.location.nameLocation;
					break;
				}
			}
		}
		if(!newValue || newValue == oldValue) return;
		$scope.data.space.id = 0;

		$scope.spaces = [];
		
		Api.campaign.space($scope.data.location.id, $scope.data.section.id).then(function(res) {
			$scope.spaces = res.data;
		});
	});

	$scope.$watch('data.space.id', function(newValue, oldValue) {
		if(newValue) {
			setSpace(newValue);
		}
		if(!newValue || newValue == oldValue) return;

		$scope.getCalendars();
	});

	function setSpace(newValue) {
		for(var k in $scope.spaces) {
			if($scope.spaces[k]._id == newValue) {
				$scope.data.space = angular.extend({}, $scope.spaces[k]);
				$scope.data.space.id = $scope.data.space._id;
				break;
			}
		}
	}

	$scope.selectDay = function(dayArr, dayInfo, weekday, calendarNum) {
		var data = $scope.calendar[calendarNum].data;
		var min = data.req_min;
		if(!min) return;
		var date = $scope.calendar[calendarNum].date;
		var m = date.getMonth();
		var y = date.getFullYear();
		var key = addZero(dayInfo.num)+'-'+addZero(m+1)+'-'+y;
		var section = $scope.data.section.id;
		if(dayArr[0] == 2) {
			var order = findOrder(key);
			if(order) {
				$scope.orders[order.section].splice(order.index, 1);
				dayArr[0] = 3;
				if(!$scope.orders[order.section].length) delete $scope.orders[order.section];
				removeIncorrectDays();
				return;
			}
		}
		if(dayArr[0] != 3) return;
		
		var selected = [{day: dayInfo.num, calendarNum: 0, date: key}];
		var nextMonthDay = 0;
		var appending = false;

		if(dayInfo.num > 1) {
			var dayData = data.daysData[dayInfo.num - 1];
			if(dayData[0] == 2) appending = true;
		}

		for(var i = 1; i <= min; i++) {
			if(appending || selected.length == min) break;
			var calNum = calendarNum;
			var dayNum = dayInfo.num + i;
			dayData = $scope.calendar[calNum].data.daysData[dayNum];
			if(!dayData) {
				calNum++;
				if(!$scope.calendar[calNum]) break;
				dayNum = ++nextMonthDay;
				dayData = $scope.calendar[calNum].data.daysData[dayNum];
				if(!dayData) break;
			}
			if(dayData[0] < 2) break;
			if(dayData[0] == 2) {
				appending = true;
				break;
			}
			if(dayData[0] == 3) {
				date = $scope.calendar[calNum].date;
				m = date.getMonth();
				y = date.getFullYear();
				key = addZero(dayNum)+'-'+addZero(m+1)+'-'+y;
				selected.push({day: dayNum, calendarNum: calNum, date: key});
			}
		}

		for(var i = 1; i <= min; i++) {
			if(appending || selected.length == min) break;
			calNum = calendarNum;
			dayNum = dayInfo.num - i;
			dayData = $scope.calendar[calNum].data.daysData[dayNum];
			if(!dayData || dayData[0] < 2) break;
			if(dayData[0] == 3) {
				date = $scope.calendar[calNum].date;
				m = date.getMonth();
				y = date.getFullYear();
				key = addZero(dayNum)+'-'+addZero(m+1)+'-'+y;
				selected.unshift({day: dayNum, calendarNum: calNum, date: key});
			}
		}
		
		if(!appending && selected.length != min) return;
		
		for(i = 0; i < selected.length; i++) {
			data = $scope.calendar[selected[i].calendarNum].data;
			var dayArray = data.daysData[selected[i].day];
			dayArray[0] = 2;
			if(!$scope.orders[section]) $scope.orders[section] = [];
			var price = data.weekdays[weekday].slots[0].price;
			if(data.specialDays[selected[i].day]) price -= price / 100 * data.specialDays[selected[i].day].price;
			$scope.orders[section].push({
				section: angular.extend({}, $scope.data.section),
				city: angular.extend({}, $scope.data.city),
				env: angular.extend({}, $scope.data.env),
				category: angular.extend({}, $scope.data.category),
				location: angular.extend({}, $scope.data.location),
				space: angular.extend({}, $scope.data.space),
				date: selected[i].date,
				slots: [{num: 0, price: price, start: data.weekdays[weekday].slots[0].start, end: data.weekdays[weekday].slots[0].end}],
			});
		}
	};

	$scope.selectSlot = function(dayArr, dayInfo, weekday, calendarNum) {
		var data = $scope.calendar[calendarNum].data;
		var date = $scope.calendar[calendarNum].date;
		var m = date.getMonth();
		var y = date.getFullYear();
		var key = addZero(dayInfo.num)+'-'+addZero(m+1)+'-'+y;
		var section = $scope.data.section.id;
		var order = findOrder(key);
		if(dayArr.indexOf(2) == -1) {
			if(order) {
				$scope.orders[order.section].splice(order.index, 1);
				if(!$scope.orders[order.section].length) delete $scope.orders[order.section];
				return;
			}
		}
		if(dayArr.indexOf(2) > -1) {
			if(!$scope.orders[section]) $scope.orders[section] = [];
			var slots = [];
			for(var k in dayArr) {
				if(dayArr[k] == 2) {
					var price = data.weekdays[weekday].slots[k].price;
					if(data.specialDays[dayInfo.num]) price -= price / 100 * data.specialDays[dayInfo.num].price;
					slots.push({num: k, price: price, start: data.weekdays[weekday].slots[k].start, end: data.weekdays[weekday].slots[k].end});
				}
			}
			var obj = {
				section: angular.extend({}, $scope.data.section),
				city: angular.extend({}, $scope.data.city),
				env: angular.extend({}, $scope.data.env),
				category: angular.extend({}, $scope.data.category),
				location: angular.extend({}, $scope.data.location),
				space: angular.extend({}, $scope.data.space),
				date: key,
				slots: slots,
			};
			if(order)
				$scope.orders[order.section][order.index] = obj;
			else
				$scope.orders[section].push(obj);
		}
	};

	function findOrder(date) {
		for(var section in $scope.orders) {
			for(var j in $scope.orders[section]) {
				var item = $scope.orders[section][j];
				if(item.date == date && 
					item.section.id == $scope.data.section.id &&
					item.city.id == $scope.data.city.id &&
					item.env.id == $scope.data.env.id &&
					item.category.id == $scope.data.category.id &&
					item.location.id == $scope.data.location.id &&
					item.space.id == $scope.data.space.id) {
					return {item: item, index: j, section: section};
				}
			}
		}
		return null;
	}

	function removeIncorrectDays() {
		var min = $scope.calendar[0].data.req_min;
		if(!min) return;
		var days = [];
		var calNum = 0;
		var day = 0;
		var dayData;
		while(true) {
			day++;
			dayData = $scope.calendar[calNum].data.daysData[day];
			if(!dayData) {
				calNum++;
				if(calNum > 1) break;
				day = 0;
				continue;
			} else {
				if(dayData[0] == 2) days.push({day: day, calendarNum: calNum});
				else {
					if(days.length < min) {
						for(var k in days) {
							var date = $scope.calendar[days[k].calendarNum].date;
							var m = date.getMonth();
							var y = date.getFullYear();
							var key = addZero(days[k].day)+'-'+addZero(m+1)+'-'+y;
							var order = findOrder(key);
							if(order) {
								$scope.orders[order.section].splice(order.index, 1);
								if(!$scope.orders[order.section].length) delete $scope.orders[order.section];
							}
							$scope.calendar[days[k].calendarNum].data.daysData[days[k].day][0] = 3;
						}
					}
					days = [];
				}
			}
		}
	}

	$scope.sumPrice = function(orders) {
		var sum = 0;
		for(var k in orders) {
			for(var j in orders[k].slots) {
				sum += orders[k].slots[j].price;
			}
		}
		return sum;
	};

	$scope.sumAllPrice = function(orders) {
		var sum = 0;
		for(var k in orders) {
			sum += $scope.sums[k];
		}
		return sum;
	};

	$scope.removeOrders = function(section, order, item) {
		if(order) {
			order.slots.splice(order.slots.indexOf(item), 1);
			if(!order.slots.length) $scope.orders[section].splice($scope.orders[section].indexOf(order), 1);
			if(!$scope.orders[section].length) delete $scope.orders[section];
		} else {
			delete $scope.orders[section];
		}
		setDaysData(0, $scope.calendar[0].data, $scope.calendar[0].date);
		setDaysData(1, $scope.calendar[1].data, $scope.calendar[1].date);
		removeIncorrectDays();
	};

	$scope.rand = function(i) {
		return i + Math.random();
	};

	$scope.getDayArray = function(wday, day, data, date) {
		var d = new Date();
		d.setHours(0,0,0,0);
		date.setHours(0,0,0,0);
		var slotsCount = data.req_min ? 1 : 4;
		var currentMonth = d.getMonth();
		var chosenMonth = date.getMonth();
		var chosenYear = date.getFullYear();
		var currentYear = d.getFullYear();
		var chosenDate = new Date(chosenYear, chosenMonth, day);
		var fill = 3;
		var currentDay = d.getDate();
		//debugger
		if((chosenYear < currentYear || 
			(chosenYear == currentYear && chosenMonth < currentMonth)) || 
			!data.weekdays[wday].enabled || data.unavailableDays[day] || 
			chosenDate.getTime() - d.getTime() < (7*3600000*24))
			fill = 0;

		var arr = fillArray(fill, data.weekdays[wday].slots ? data.weekdays[wday].slots.length : slotsCount);
		if(data.bookedDays[day]) {
			for(var i = 0; i < data.bookedDays[day].length; i++) {
				var slot = data.bookedDays[day][i];
				arr[slot ? slot - 1 : 0] = 1;
			}
		}
		if(data.busyDays[day]) {
			for(var i = 0; i < data.busyDays[day].length; i++) {
				var slot = data.busyDays[day][i];
				arr[slot ? slot - 1 : 0] = 0;
			}
		}

		var order = findOrder(addZero(day)+'-'+addZero(chosenMonth+1)+'-'+chosenYear);
		if(order) {
			for(i in order.item.slots) {
				arr[order.item.slots[i].num] = 2;
			}
		}

		return arr;
	};

	$scope.parseData = function(calendar, date, calendarNum) {
		var y = date.getFullYear();
		var m = date.getMonth();
		var settings = $scope.data.space.arrayActivitySettings[0];
		var res = {};
		var weekdays = [];
		
		res.req_min = parseInt(settings.req_min) || 0;
		
		for(var i = 0; i < 7; i++) {
			var obj = {};

			obj.enabled = settings.weekday[i].chk;
			if(!obj.enabled) {
				weekdays.push(obj);
				continue;
			}

			var slots = [];
			var slotsCount = res.req_min ? 1 : 4;
			for(var j = 1; j <= slotsCount; j++) {
				if(+settings.weekday[i]['slot_'+j] > 0) {
					slots.push({num: j, price: +settings.weekday[i]['slot_'+j]});
				}
			}

			if(!slots.length) {
				obj.enabled = false;
				weekdays.push(obj);
				continue;
			} else obj.slots = slots;
			
			if(!res.req_min) {
				var time = settings.weekday[i].start.split(':');
				var start = new Date(y, m, 1, time[0], time[1]);
				
				time = settings.weekday[i].end.split(':');
				var end = new Date(y, m, 1, time[0], time[1]);

				var startTime = start.getTime();
				var between = end.getTime() - startTime;
				var piece = Math.round(between / slots.length);


				for(j = 0; j < obj.slots.length; j++) {
					var slotStart = new Date(startTime);
					startTime += piece;
					var slotEnd = new Date(startTime);
					obj.slots[j].start = addZero(slotStart.getHours()) + ':' + addZero(slotStart.getMinutes());
					obj.slots[j].end = addZero(slotEnd.getHours()) + ':' + addZero(slotEnd.getMinutes());
				}
			} else {
				obj.slots[0].start = settings.weekday[i].start;
				obj.slots[0].end = settings.weekday[i].end;
			}

			weekdays.push(obj);

		}
		res.weekdays = weekdays;

		var unavailable = calendar.unavailability;
		var unavailableDays = {};
		for(i = 0; i < unavailable.length; i++) {
			for(j = 0; j < unavailable[i].date.length; j++) {
				unavailableDays[unavailable[i].date[j]] = {message: unavailable[i].message, reason: unavailable[i].reason};
			}
		}
		res.unavailableDays = unavailableDays;

		var special = calendar.specialDays;
		var specialDays = {};
		for(i = 0; i < special.length; i++) {
			for(j = 0; j < special[i].date.length; j++) {
				specialDays[special[i].date[j]] = {price: special[i].price};
			}
		}
		res.specialDays = specialDays;

		var booked = calendar.booked;
		var bookedDays = {};
		for(i = 0; i < booked.length; i++) {
			var day = parseInt(booked[i].date);
			if(!bookedDays[day]) bookedDays[day] = [];
			var num = booked[i].slot ? booked[i].slot.split('_')[1] : null;
			bookedDays[day].push(num);
		}
		res.bookedDays = bookedDays;

		var busy = calendar.notAvailable;
		var busyDays = {};
		for(i = 0; i < busy.length; i++) {
			var day = parseInt(busy[i].date);
			if(!busyDays[day]) busyDays[day] = [];
			var num = busy[i].slot ? busy[i].slot.split('_')[1] : null;
			busyDays[day].push(num);
		}
		res.busyDays = busyDays;

		res.daysData = getDaysData(calendarNum, res, date);

		return res;

	};

	function getDaysData(calendarNum, data, date) {
		var res = {};
		for(i = 0; i < Math.ceil($scope.calendar[calendarNum].items.length / 7); i++) {
			for(j = 0; j < 7; j++) {
				if(!$scope.calendar[calendarNum].items[j+(i*7)].another)
					res[$scope.calendar[calendarNum].items[j+(i*7)].num] = $scope.getDayArray(j, $scope.calendar[calendarNum].items[j+(i*7)].num, data, date);
			}
		}
		return res;
	}

	function setDaysData(calendarNum, data, date) {
		for(i = 0; i < Math.ceil($scope.calendar[calendarNum].items.length / 7); i++) {
			for(j = 0; j < 7; j++) {
				if(!$scope.calendar[calendarNum].items[j+(i*7)].another)
					data.daysData[$scope.calendar[calendarNum].items[j+(i*7)].num] = $scope.getDayArray(j, $scope.calendar[calendarNum].items[j+(i*7)].num, data, date);
			}
		}
	}

	setSpace($scope.data.space.id);
	if($scope.data.space.id) $scope.getCalendars();

	function addZero(num) {
		return num < 10 ? '0' + num : num;
	}

	$scope.nameEditingDone = function(e) {
		if(e.keyCode == 13) $scope.data.nameEditing = false;
	};

	function fillArray(value, len) {
	  var arr = [];
	  for (var i = 0; i < len; i++) {
	    arr.push(value);
	  }
	  return arr;
	}

	function calcCalendar(date) {
		var buf;
		var month = date.getMonth();
		var year = date.getFullYear();

		buf = new Date();
		if(buf.getFullYear() == year && buf.getMonth() == month)
			var today = buf.getDate();

		buf = new Date(year, month + 1, 0);
		var lastWeekDay = buf.getDay() || 7;
		var lastDay = buf.getDate();
		var firstWeekDay = new Date(year, month, 1).getDay() || 7;

		var beforeCount = firstWeekDay - 1;
		var afterCount = 7 - lastWeekDay;
		var prevMonthLastDay = new Date(month == 0 ? year - 1 : year, month, 0).getDate();

		var res = [];
		for(var i = prevMonthLastDay - beforeCount + 1; i <= prevMonthLastDay; i++)
			res.push({num: i, another: true});
		for(var i = 1; i <= lastDay; i++) {
			var obj = {num: i};
			res.push(obj);
		}
		for(var i = 1; i <= afterCount; i++)
			res.push({num: i, another: true});

		return {items: res, month: months[month], year: year, date: date};
	}

	$scope.prevMonth = function() {
		var date = $scope.calendar[0].date;
		$scope.calendar[1] = $scope.calendar[0];
		var month = date.getMonth();
		var year = date.getFullYear();
		date = new Date(month == 0 ? year - 1 : year, month == 0 ? 11 : month - 1, 1);
		$scope.calendar[0] = calcCalendar(date);

		$scope.getCalendar(0, date);
	};
	$scope.nextMonth = function() {
		var date = $scope.calendar[1].date;
		$scope.calendar[0] = $scope.calendar[1];
		var month = date.getMonth();
		var year = date.getFullYear();
		date = new Date(month == 11 ? year + 1 : year, month == 11 ? 0 : month + 1, 1);
		$scope.calendar[1] = calcCalendar(date);

		$scope.getCalendar(1, date);
	};

}]);