ExterionLive.controller('ApplicationCtrl', ['$scope', 'Api', '$state', '$location', 'ModalService', 'AuthService', 'Globals', function ($scope, Api, $state, $location, ModalService, AuthService, Globals) {
    
    $scope.Globals = Globals;
    $scope.state = $state;
    $scope.user = AuthService.getUser() || {};
    $scope.orders = {env: 0, reach: 0, booking: 0, campaign: []};
    $scope.section = [];
    Api.main.section().then(function(data) {
        $scope.section = data.data;
    });
    /*$scope.logout = function() {
        AuthService.logout();
        AuthService.toLoginPage();
    };*/

    $scope.loginModal = function(data, afterSave, afterClose) {
        var c_data = angular.merge({}, data);
        ModalService.showModal({
            templateUrl: '/pages/login/login.html',
            controller: 'LoginCtrl',
            inputs: {
                data: c_data,
                afterSave: function(res) {
                    if(afterSave) afterSave(res, data);
                }
            }
        }).then(function (modal) {
            Globals.modalOpen = true;
            //$.fn.fullpage.setAutoScrolling(false);
            modal.close.then(function (result) {
                Globals.modalOpen = false;
                if(result) $scope.user = result;
                //$.fn.fullpage.setAutoScrolling(true);
                if(afterClose) afterClose(result);
            });
        });
    };

    $scope.calcPagination = function(current, count) {
        var res = [];
        var add, may, i;
        var left = current > 3 ? 3 : current - 1;
        var right = current < count - 2 ? 3 : count - current;
        if(left < 3 && right == 3) {
            add = 3 - left;
            may = count - (current + right);
            add = may > add ? add : may;
            right += add;
        }
        if(right < 3 && left == 3) {
            add = 3 - right;
            may = current - left - 1;
            add = may > add ? add : may;
            left += add;
        }
        res.push({num: current});
        for(i = current - 1; i >= current - left; i--) {
            res.unshift({num: i});
        }
        for(i = current + 1; i <= current + right; i++) {
            res.push({num: i});
        }
        if(current - left > 1) {
            if(current - left > 2) res.unshift({divider: true});
            res.unshift({num: 1});
        }
        if(current + right < count) {
            if(current + right < count - 1) res.push({divider: true});
            res.push({num: count});
        }
        return res;
    };

    $scope.isActive = function (path) {
        if ($location.path().substr(0, path.length) === path) {
            if (path === "/" && $location.path() === "/") {
                return true;
            } else if (path === "/") {
                return false;
            } else {
                return true;
            }
        } else {
            return false;
        }
    };

}]);