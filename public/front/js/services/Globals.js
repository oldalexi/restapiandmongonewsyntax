ExterionLive.service('Globals', ['$q', function($q) {
    return {
        readFile: function(file) {
            var deferred = $q.defer();
            var reader = new FileReader();
            reader.onloadend = function(e) {
                var blob = new Blob([e.target.result]);
                deferred.resolve(URL.createObjectURL(blob));
            };
            reader.readAsArrayBuffer(file);
            return deferred.promise;
        },
        fixedEncodeURIComponent: function(str) {
          return encodeURI(str).replace(/[!'()*]/g, function(c) {
            return '%' + c.charCodeAt(0).toString(16);
          });
        },
        path: {
            host: '',
        },
        getFullUrl: function(url) {
            return this.path.host + this.fixedEncodeURIComponent(url);
        },
        coverMiddle: '/resources/img/cover-middle.jpg',
        coverBig: '/resources/img/cover-big.jpg',
        modalOpen: false,
        strings: {
            required: 'Please enter a value for this field.',
        },
    }
}]);
