ExterionLive.service('Alerts', ['ModalService', 'Globals', '$q', function(ModalService, Globals, $q) {
	var self = this;
	this.confirm = function(params) {
		ModalService.showModal({
		    templateUrl: 'views/alerts/confirm.html',
		    controller: ['$scope', 'params', 'close', function($scope, params, close) {
		    	$scope.params = params;
		    	$scope.close = function() {
		    		close();
		    	};
		    	$scope.action = function() {
		    		params.action();
		    		close();
		    	};
		    }],
		    //appendElement: angular.element(document.getElementsByTagName('main')),
		    inputs: {
		    	params: params
		    }
		}).then(function (modal) {
			Globals.modalOpen = true;
		    modal.close.then(function (result) {
		    	Globals.modalOpen = false;
		    	if(params.afterClose) params.afterClose(result);
		    });
		});
	};

	this.alert = function(params) {
		self.confirm(angular.merge(params, {hideAction: true, isAlert: true, title: 'Attention'}));
	};

	this.choose = function(items, field) {
		var deferred = $q.defer();
		ModalService.showModal({
		    templateUrl: 'views/alerts/choose.html',
		    controller: ['$scope', 'items', 'field', 'close', function($scope, items, field, close) {
		    	$scope.items = items;
		    	$scope.field = field;
		    	$scope.close = function() {
		    		close();
		    	};
		    	$scope.choose = function(item) {
		    		close(item);
		    	};
		    }],
		    inputs: {
		    	items: items,
		    	field: field || 'name'
		    }
		}).then(function (modal) {
			Globals.modalOpen = true;
		    modal.close.then(function (result) {
		    	Globals.modalOpen = false;
		    	if(result) deferred.resolve(result);
		    	else deferred.reject();
		    });
		});
		return deferred.promise;
	};

	return this;
}]);