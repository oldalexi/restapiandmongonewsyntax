ExterionLive.factory('Api', ['$http', 'AuthService', 'Globals', function($http, AuthService, Globals) {
	// TODO: refact
	function toQueryString(obj) {
	  var str = [];
	  for(var p in obj)
	    if (obj.hasOwnProperty(p) && obj[p] != undefined) {
	      str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
	    }
	  return str.join("&");
	}

	function toFormData(obj, form, namespace) {

	  var fd = form || new FormData();
	  var formKey, value;

	  for(var property in obj) {
	    if(obj.hasOwnProperty(property)) {

	      if(namespace) {
	        formKey = namespace + '[' + property + ']';
	      } else {
	        formKey = property;
	      }

	      // if the property is an object, but not a File,
	      // use recursivity.
	      if(typeof obj[property] === 'object' && !(obj[property] instanceof File) && !(obj[property] instanceof Date)) {

	        toFormData(obj[property], fd, formKey);

	      } else {
	      	value = obj[property];
	      	if(value instanceof Date)
	        	value = value.toLocaleDateString();
	        // if it's a string or a File object
	        fd.append(formKey, value);
	      }

	    }
	  }

	  return fd;

	}
	
	var host = Globals.path.host + '/';
	var base = host + 'administrator/';

	return {
		main: {
			section: function() {
				return $http.post(base + '', {flag: 'getAll'});
			},
			intro: function() {
				return $http.post(base + '', {flag: 'getAll', page: 'main'});
			},
			images: function(ids) {
				return $http.post(base + '', {arrayId: ids});
			},
		},
		cases: {
			getAll: function() {
				return $http.post(base + '', {flag: 'getAll'});
			},
			intro: function() {
				return $http.post(base + '', {flag: 'getAll', page: 'cases'});
			},
		},
		contacts: {
			getFaq: function() {
				return $http.post(base + '', {flag: 'getFaq'});
			},
			intro: function() {
				return $http.post(base + '', {flag: 'getAll', page: 'contact'});
			},
			phoneNumbers: function() {
				return $http.post(base + '', {flag: 'getPhoneNumbers'});
			},
			contactInfo: function() {
				return $http.post(base + '', {flag: 'getContactInfo'});
			},
			emails: function() {
				return $http.post(base + '', {flag: 'getEmails'});
			},
		},
		experientialServices: {
			intro: function() {
				return $http.post(base + '', {flag: 'getAll', page: "externalServices"});
			},
			experiential_services: function() {
				return $http.post(base + '', {flag: 'getAll'});
			},
		},

		campaign: {
			section: function(search) {
				var flag = search ? 'searchById' : 'getAllActivity';
				var data = {'flag': flag};
				if(search) data = angular.merge(data, {on: 'activity', searchString: search});
				return $http.post(host + 'user/api/campaign', data);
			},
			city: function(section, search) {
				var flag = search ? 'searchById' : 'getAllCityById';
				var data = {'flag': flag, id: section};
				if(search) data = angular.merge(data, {on: 'city', searchString: search});
				return $http.post(host + 'user/api/campaign', data);
			},
			env: function(city) {
				return $http.post(host + 'user/api/campaign', {flag: 'getAllEnvironmentById', id: city});
			},
			category: function(city, env) {
				return $http.post(host + 'user/api/campaign', {flag: 'getAllCategoriesById', id_e: env, id_c: city});
			},
			location: function(city, env, category, search) {
				var flag = search ? 'searchById' : 'getAllLocationsById';
				var data = {'flag': flag, id_e: env, id_c: city, id_ca: category};
				if(search) data = angular.merge(data, {on: 'location', searchString: search});
				return $http.post(host + 'user/api/campaign', data);
			},
			space: function(location, section, search) {
				var flag = search ? 'searchById' : 'getAllSpaceById';
				var data = {'flag': flag, id: location, idActivity: section};
				if(search) data = angular.merge(data, {on: 'space', searchString: search});
				return $http.post(host + 'user/api/campaign', data);
			},
			calendar: function(space, section, m, y) {
				if(++m < 10) m = '0' + m;
				return $http.post(host + 'user/calendar', {flag: 'getAllOrderInMonth', id: space, date: m + '-' + y, idActivity: section});
			},
		},
		users: {
			login: function(data) {
				return $http.post(host + 'login', data);
			},
			signup: function(data) {
				return $http.post(host + 'signup', data);
			},
			vat: function(vat) {
				return $http.post(host + 'user/search_vat', {'vat': vat});
			}
		},
	}
}]);
