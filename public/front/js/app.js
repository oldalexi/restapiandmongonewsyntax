window.ExterionLive = angular.module('ExterionLive', ['ui.router', 'ngMessages', 'angularModalService', 'LocalStorageModule', 'checklist-model', 'ngMask', 'ngAnimate'])

    .run(['AuthService', '$rootScope', function (AuthService, $rootScope) {

        $rootScope.$on('$stateChangeStart',
        function(event, toState, toParams, fromState, fromParams, options){
            //$('.preloader').fadeIn('fast');
            $('.megaMenu').hide();
            $('.mainContent').addClass('slide-up');
        });

        $rootScope.$on('$stateChangeSuccess',
        function(event, toState, toParams, fromState, fromParams, options){
            $('body').scrollTop(0);
            $('.preloader').hide();
            $('.mainContent').removeClass('slide-up');
        });

        $rootScope.$on('$stateChangeError',
        function(event, toState, toParams, fromState, fromParams, options){
            $('.preloader').hide();
            $('.mainContent').removeClass('slide-up');
        });

        //if(AuthService.isAuthenticated()) AuthService.setAuthHeader();

    }])

    .factory('httpInterceptor', ['$q', '$injector', function($q, $injector) {
      return {
        responseError: function(rejection) {
            var AuthService = $injector.get('AuthService');
            if (rejection.status == 401) {
                AuthService.logout();
                AuthService.toLoginPage();
            }
            return $q.reject(rejection);
        }
      };
    }])

    .config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$compileProvider', '$httpProvider', function ($stateProvider, $urlRouterProvider, $locationProvider, $compileProvider, $httpProvider) {

        //$compileProvider.debugInfoEnabled(false);

        // $httpProvider.defaults.headers.common = {};
        // $httpProvider.defaults.headers.post = {};
        // $httpProvider.defaults.headers.put = {};
        // $httpProvider.defaults.headers.patch = {};

        $httpProvider.interceptors.push('httpInterceptor');

        $locationProvider.html5Mode(true);

        var pages = '/pages/';
        $urlRouterProvider.otherwise(function ($injector) {
            var $state = $injector.get('$state');
            $state.go('index');
        });

        $stateProvider
            .state('index', {
                url: '/',
                templateUrl: pages + 'home/home.html',
                controller: 'HomeCtrl',
                resolve: {
                    section: ['Api', function(Api) {
                        return Api.main.section();
                    }],
                    intro: ['Api', function(Api) {
                        return Api.main.intro();
                    }],
                    images: ['Api', 'section', 'intro', function(Api, section, intro) {
                        var ids = intro.data.image;
                        for(var k in section.data) {
                            ids = ids.concat(section.data[k].image);
                        }
                        return Api.main.images(ids);
                    }],
                }
            })
            .state('cases', {
                resolve: {
                    section: ['Api', function(Api) {
                        return Api.main.section();
                    }],
                    cases: ['Api', function(Api) {
                        return Api.cases.getAll();
                    }],
                    intro: ['Api', function(Api) {
                        return Api.cases.intro();
                    }],
                    images: ['Api', 'section', 'intro', 'cases', function(Api, section, intro, cases) {
                        var ids = [];
                        for(var k in intro.data.image) {
                            ids = ids.concat(intro.data.image[k]);
                        }
                        for(var k in cases.data) {

                            if(cases.data[k].publish != true) {
                                cases.data.splice(k,1);
                                if(k != 0) {
                                    k--;
                                }
                                continue;
                            }
                            for(var j in cases.data[k].mainImage) {
                                ids = ids.concat(cases.data[k].mainImage[j]);
                            }

                        }
                        return Api.main.images(ids);
                    }],
                },
                url: '/cases',
                controller: 'CasesCtrl',
                templateUrl: pages + 'cases/cases.html',
            })
            .state('cases.detail', {
                resolve: {
                    $case: ['$stateParams', 'cases', function($stateParams, cases) {
                        var cs = cases.data.find(function(cs, key) {
                            if(cs._id == $stateParams._id) {
                                cs['key'] = key
                                return true;
                            }
                            return false;
                        });
                        if(cases.data.length  == 1) return cs;
                        else if(cases.data.length == 2) {
                            if(cs.key == 0) {
                                cs['next'] = cases.data[1];
                            } else {
                                cs['prev'] = cases.data[0];
                            }
                        } else {
                            if(cs.key != cases.data.length-1)
                                cs['next'] = cases.data[cs.key+1];
                            cs['prev'] = cases.data[cs.key-1];
                        }
                        return cs;
                    }],
                    images: ['Api', '$case', function(Api, $case){
                        var ids = [];
                        for(var k in $case.casesImage) {
                            ids = ids.concat($case.casesImage[k]);
                        }
                        return Api.main.images(ids);
                    }],
                 },
                url: '/{_id:string}/detail',
                controller: 'CasesDetailCtrl',
                templateUrl: pages + 'cases/cases_detail.html',
                params: {_id: null},
            })
            .state('contacts', {
                resolve: {
                    intro: ['Api', function(Api) {
                        return Api.contacts.intro();
                    }],
                    faqs: ['Api', function(Api) {
                        return Api.contacts.getFaq();
                    }],
                    phoneNumbers: ['Api', function(Api) {
                        return Api.contacts.phoneNumbers();
                    }],
                    emails: ['Api', function(Api) {
                        return Api.contacts.emails();
                    }],
                    contactInfo: ['Api', function(Api) {
                        return Api.contacts.contactInfo();
                    }],
                    images: ['Api', 'intro', function(Api, intro) {
                        var ids = intro.data.image;
                        return Api.main.images(ids);
                    }],
                },
                url: '/contacts',
                controller: 'ContactsCtrl',
                templateUrl: pages + 'contacts/contacts.html',
            })
            .state('experiential', {
                resolve: {
                    cities: ['Api', '$stateParams', function(Api, $stateParams) {
                        return Api.experiential.cities($stateParams._id);
                    }],
                    sectionInfo: ['Api', '$stateParams', function(Api, $stateParams) {
                        return Api.experiential.sectionInfo($stateParams._id);
                    }],
                    features: ['Api', '$stateParams', function(Api, $stateParams) {
                        return Api.experiential.getAllCityByIdWithFeatured($stateParams._id);
                    }],
                    images: ['Api', 'sectionInfo', function(Api, sectionInfo) {
                        var ids = [];
                        for(var k in sectionInfo.data.image) {
                            ids = ids.concat(sectionInfo.data.image[k]);
                        }

                        return Api.main.images(ids);
                    }],
                },
                url: '/{_id:string}/experiential',
                controller: 'ExperientialCtrl',
                templateUrl: pages + 'experiential/experiential.html',
            })
            .state('experiential.environment', {
                resolve: {
                    environments: ['Api', '$stateParams', function(Api, $stateParams) {
                        return Api.experiential.env($stateParams.city_id);
                    }]
                },
                url: '/{city_id:string}/environment',
                controller: 'ExperientialEnvironmentCtrl',
                templateUrl: pages + 'experiential/environment.html',
            })
            .state('experiential.environment.category', {
                resolve: {
                    categories: ['Api', '$stateParams', function(Api, $stateParams) {
                        return Api.experiential.category($stateParams.city_id, $stateParams.environment_id);
                    }],
                },
                url: '/{environment_id:string}/category',
                controller: 'ExperientialCategoryCtrl',
                templateUrl: pages + 'experiential/category.html',
            })
            .state('experiential.environment.category.location', {
                resolve: {
                    locations: ['Api', '$stateParams', function(Api, $stateParams) {
                        return Api.experiential.location($stateParams.city_id, $stateParams.environment_id, $stateParams.category_id);
                    }],
                },
                url: '/{category_id:string}/location',
                controller: 'ExperientialLocationCtrl',
                templateUrl: pages + 'experiential/location.html',
            })
            .state('experiential.environment.category.location.spaces', {
                resolve: {
                    spaces: ['Api', '$stateParams', function(Api, $stateParams) {

                        return Api.experiential.getInfoLocationAndSpace($stateParams.location_id);
                    }],
                },
                url: '/{location_id:string}/spaces',
                controller: 'ExperientialSpacesCtrl',
                templateUrl: pages + 'experiential/spaces.html',
            })
            .state('experiential.space', {
                resolve: {
                    city: ['Api', '$stateParams', 'cities', function(Api, $stateParams, cities) {
                        
                        return cities.data.find(function(city){
                            return city._id == $stateParams.city_id;
                        });
                    }],
                    space: ['Api', '$stateParams', function(Api, $stateParams) {
                        debugger
                        return Api.experiential.getInfoSpace($stateParams.space_id);
                    }],
                },
                url: '/{space_id:string}/space/{city_id:string}/{location:string}?{params:json}',
                controller: 'ExperientialSpaceCtrl',
                templateUrl: pages + 'experiential/space.html',
                params: {city_id:null, location: null, params: null}
            })
            .state('experiential_services', {
                resolve: {
                    intro: ['Api', function(Api) {
                        return Api.experientialServices.intro();
                    }],
                    experiential_services: ['Api', function(Api) {
                        return Api.experientialServices.experiential_services();
                    }],
                    images: ['Api', 'intro', 'experiential_services', function(Api, intro, experiential_services) {
                        var ids = [];
                        for(var j in intro.data.image) {
                            ids = ids.concat(intro.data.image[j]);
                        }

                        if(angular.isArray(experiential_services.data)) {
                            for(var k in experiential_services.data) {
                                ids = ids.concat(experiential_services.data[k].image);
                            }
                        }
                        return Api.main.images(ids);
                    }],
                },
                url: '/experiential_services',
                controller: 'ExperientialServicesCtrl',
                templateUrl: pages + 'experiential_services/experiential_services.html',
            })
            .state('info', {
                resolve: {
                    intro: ['Api', function(Api) {
                        return Api.info.intro();
                    }],
                    info: ['Api', function(Api) {
                        return Api.info.getAll();
                    }],
                },
                url: '/info_and_pricing',
                templateUrl: pages + 'info/info.html',
                controller: 'InfoCtrl',
            })
            .state('campaign_main', {
                url: '/create_campaign',
                templateUrl: pages + 'campaign/main.html',
                controller: 'CampaignMainCtrl',
            })
            .state('campaign_section', {
                url: '/create_campaign_section?data',
                templateUrl: pages + 'campaign/section.html',
                controller: 'CampaignSectionCtrl',
                resolve: {
                    sections: ['Api', function(Api) {
                        return Api.campaign.section();
                    }],
                }
            })
            .state('campaign_premade', {
                url: '/create_campaign_premade',
                templateUrl: pages + 'campaign/premade.html',
                controller: 'CampaignPremadeCtrl',
            })
            .state('campaign_city', {
                url: '/create_campaign_city?data',
                templateUrl: pages + 'campaign/city.html',
                controller: 'CampaignCityCtrl',
                resolve: {
                    cities: ['Api', '$stateParams', function(Api, $stateParams) {
                        var data = JSON.parse(b64_to_utf8($stateParams.data || '') || '{}');
                        if(!data.section) return {data: []};
                        return Api.campaign.city(data.section.id);
                    }],
                }
            })
            .state('campaign_env', {
                url: '/create_campaign_environment?data',
                templateUrl: pages + 'campaign/env.html',
                controller: 'CampaignEnvCtrl',
                resolve: {
                    envs: ['Api', '$stateParams', function(Api, $stateParams) {
                        var data = JSON.parse(b64_to_utf8($stateParams.data || '') || '{}');
                        if(!data.city) return {data: []};
                        return Api.campaign.env(data.city.id);
                    }],
                }
            })
            .state('campaign_category', {
                url: '/create_campaign_category?data',
                templateUrl: pages + 'campaign/category.html',
                controller: 'CampaignCategoryCtrl',
                resolve: {
                    categories: ['Api', '$stateParams', function(Api, $stateParams) {
                        var data = JSON.parse(b64_to_utf8($stateParams.data || '') || '{}');
                        if(!data.city || !data.env) return {data: []};
                        return Api.campaign.category(data.city.id, data.env.id);
                    }],
                }
            })
            .state('campaign_location', {
                url: '/create_campaign_location?data',
                templateUrl: pages + 'campaign/location.html',
                controller: 'CampaignLocationCtrl',
                resolve: {
                    locations: ['Api', '$stateParams', function(Api, $stateParams) {
                        var data = JSON.parse(b64_to_utf8($stateParams.data || '') || '{}');
                        if(!data.city || !data.env || !data.category) return {data: []};
                        return Api.campaign.location(data.city.id, data.env.id, data.category.id);
                    }],
                }
            })
            .state('campaign_space', {
                url: '/create_campaign_space?data',
                templateUrl: pages + 'campaign/space.html',
                controller: 'CampaignSpaceCtrl',
                resolve: {
                    spaces: ['Api', '$stateParams', function(Api, $stateParams) {
                        var data = JSON.parse(b64_to_utf8($stateParams.data || '') || '{}');
                        if(!data.location) return {data: []};
                        return Api.campaign.space(data.location.id, data.section.id);
                    }],
                }
            })
            .state('campaign_planner', {
                url: '/create_campaign_planner?data',
                templateUrl: pages + 'campaign/planner.html',
                controller: 'CampaignPlannerCtrl',
                resolve: {
                    sections: ['Api', function(Api) {
                        return Api.campaign.section();
                    }],
                    cities: ['Api', '$stateParams', function(Api, $stateParams) {
                        var data = JSON.parse(b64_to_utf8($stateParams.data || '') || '{}');
                        if(!data.section) return {data: []};
                        return Api.campaign.city(data.section.id);
                    }],
                    envs: ['Api', '$stateParams', function(Api, $stateParams) {
                        var data = JSON.parse(b64_to_utf8($stateParams.data || '') || '{}');
                        if(!data.city) return {data: []};
                        return Api.campaign.env(data.city.id);
                    }],
                    categories: ['Api', '$stateParams', function(Api, $stateParams) {
                        var data = JSON.parse(b64_to_utf8($stateParams.data || '') || '{}');
                        if(!data.city || !data.env) return {data: []};
                        return Api.campaign.category(data.city.id, data.env.id);
                    }],
                    locations: ['Api', '$stateParams', function(Api, $stateParams) {
                        var data = JSON.parse(b64_to_utf8($stateParams.data || '') || '{}');
                        if(!data.city || !data.env || !data.category) return {data: []};
                        return Api.campaign.location(data.city.id, data.env.id, data.category.id);
                    }],
                    spaces: ['Api', '$stateParams', function(Api, $stateParams) {
                        var data = JSON.parse(b64_to_utf8($stateParams.data || '') || '{}');
                        if(!data.location) return {data: []};
                        return Api.campaign.space(data.location.id, data.section.id);
                    }],
                    calendar: ['Api', '$stateParams', function(Api, $stateParams) {
                        var data = JSON.parse(b64_to_utf8($stateParams.data || '') || '{}');
                        if(!data.space) return {data: null};
                        var date = new Date();
                        return Api.campaign.calendar(data.space.id, data.section.id, date.getMonth(), date.getFullYear());
                    }],
                }
            });
    }])

    .filter('capitalize', function() {
      return function(input, scope) {
        if (input!=null)
        input = input.toLowerCase();
        return input.substring(0,1).toUpperCase()+input.substring(1);
      }
    })

    .filter('unique', function() {
       return function(collection, keyname) {
          var output = [],
              keys = [];
              keyname = keyname.split(',');

          angular.forEach(collection, function(item) {
              var key = [];
              for(var k in keyname) key.push(item[keyname[k].trim()]);
              key = key.join('|');
              if(keys.indexOf(key) === -1) {
                  keys.push(key);
                  output.push(item);
              }
          });

          return output;
       };
    })

    .filter('range', function() {
      return function(input, total) {
        total = parseInt(total);

        for (var i=0; i<total; i++) {
          input.push(i);
        }
        return input;
      };
    })

    .directive('disableDrop', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                angular.element(document).on('drop dragover', function (event) {
                    var doPrevent = false;
                    var d = event.srcElement || event.target;
                    if ((d.tagName.toUpperCase() === 'INPUT' &&
                         (
                             d.type.toUpperCase() === 'TEXT' ||
                             d.type.toUpperCase() === 'PASSWORD' ||
                             d.type.toUpperCase() === 'FILE' ||
                             d.type.toUpperCase() === 'SEARCH' ||
                             d.type.toUpperCase() === 'EMAIL' ||
                             d.type.toUpperCase() === 'NUMBER' ||
                             d.type.toUpperCase() === 'DATE' )
                         ) ||
                         d.tagName.toUpperCase() === 'TEXTAREA') {
                        doPrevent = d.readOnly || d.disabled;
                    }
                    else {
                        doPrevent = true;
                    }

                    if (doPrevent) {
                        event.preventDefault();
                    }
                });
            }
        }
    })

    .directive('ngHtml', ['$compile', function($compile) {
        return function(scope, elem, attrs) {
            var watch = true;
            if(attrs.ngHtml){
                if(attrs.ngHtml.indexOf('::') === 0) {
                    watch = false;
                    attrs.ngHtml = attrs.ngHtml.substr(2);
                }
                elem.html(scope.$eval(attrs.ngHtml));
                $compile(elem.contents())(scope);
            }
            if(watch) {
                scope.$watch(attrs.ngHtml, function(newValue, oldValue) {
                    if (newValue && newValue !== oldValue) {
                        elem.html(newValue);
                        $compile(elem.contents())(scope);
                    }
                });
            }
        };
    }])

    .directive('disableBackspace', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                angular.element(document).on('keydown', function (event) {
                    var doPrevent = false;
                    if (event.keyCode === 8) {
                        var d = event.srcElement || event.target;
                        if ((d.tagName.toUpperCase() === 'INPUT' &&
                             (
                                 d.type.toUpperCase() === 'TEXT' ||
                                 d.type.toUpperCase() === 'PASSWORD' ||
                                 d.type.toUpperCase() === 'FILE' ||
                                 d.type.toUpperCase() === 'SEARCH' ||
                                 d.type.toUpperCase() === 'EMAIL' ||
                                 d.type.toUpperCase() === 'TEL' ||
                                 d.type.toUpperCase() === 'NUMBER' ||
                                 d.type.toUpperCase() === 'DATE' )
                             ) ||
                             d.tagName.toUpperCase() === 'TEXTAREA') {
                            doPrevent = d.readOnly || d.disabled;
                        }
                        else {
                            doPrevent = true;
                        }
                    }

                    if (doPrevent) {
                        event.preventDefault();
                    }
                });
            }
        }
    })

    .directive('stopEvent', function () {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                if(attr && attr.stopEvent) {
                    var ev = attr.stopEvent.toLowerCase() == 'click' ? attr.stopEvent + ' touchend' : attr.stopEvent;
                    element.on(ev, function (e) {
                        e.preventDefault();
                        e.stopPropagation();
                    });
                }
            }
        };
    })

    .directive('dropText', function () {
        return {
            restrict: 'A',
            require:'ngModel',
            link: function (scope, element, attr, ngModel) {
                element[0].ondrop = function(e) {
                    var droppedHTML = e.dataTransfer.getData("text/html");
                    if(droppedHTML) droppedHTML = angular.element(droppedHTML);
                    else return;
                    e.preventDefault();
                    scope.$apply(function(){
                      ngModel.$setViewValue(droppedHTML.text().trim());
                      ngModel.$render();
                    });
                };
            }
        }
    })

    .directive('autoFocus', function () {
        return {
            restrict: 'A',
            scope: {
                value: '=autoFocus'
            },
            link: function (scope, element, attr) {
                if(scope.value.length) element.focus();
            }
        }
    })

    .directive('select', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            scope: {
                model: '=select',
                title: '@selectTitle',
                items: '=selectItems',
                key: '@selectItemsKey',
            },
            link: function (scope, element, attr) {
                $timeout(function() {
                    element.children('span').text(scope.title);
                    element.click(function(e){
                        e.stopPropagation();
                        $('.selectInput').not(this).removeClass('open');
                        $(this).toggleClass('open');
                    });
                    element.on('click', 'li', function(e){
                        e.stopPropagation();
                        var elem = $(this);
                        scope.$apply(function() {scope.model = elem.data('value')});
                        //element.children('span').text(elem.text());
                        element.removeClass('open');
                    });
                    scope.$watch('model', function(newValue, oldValue) {
                        if(!newValue) return;
                        element.children('span').text(element.find('[data-value="'+newValue+'"]').text());
                    });
                    scope.$watch('items', function(newValue, oldValue) {
                        if(!newValue || newValue == oldValue) return;
                        scope.model = null;
                        element.children('span').text(scope.title);
                    });
                });
            }
        }
    }])
    .directive('closeSelects', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                element.click(function() {
                    $('.selectInput').removeClass('open');
                });
            }
        }
    }])
    .directive('fullWidthSlider', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            scope: {
                value: '=fullWidthSlider'
            },
            link: function (scope, element, attr) {
                $timeout(function() {
                    element.slick({
                        dots: false,
                        infinite: true,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        nextArrow: '<div class="slickArr slick-next">NE<br/>XT</div>',
                        prevArrow: '<div class="slickArr slick-prev">PR<br/>EV</div>',
                        adaptiveHeight: true,
                        responsive: true
                    });
                    //$(window).trigger('resize');
                });
            }
        }
    }])
    .directive('gridWrapper', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                function init() {
                    var elem = element.parent();
                    if(elem.data('masonry-active')) {
                        elem.masonry('prepended', elem.find('.expTypeItem'));
                        return;
                    }
                    elem.masonry({
                        itemSelector: '.expTypeItem',
                        percentPosition: true,
                        gutter: 30,
                        fitWidth: true,
                        gutterWidth: 50,
                        fitWidth: true,
                    }).data('masonry-active', '1');
                }
                if(scope.$last) {
                    $timeout(function() {
                        init();
                    });
                }
            }
        }
    }])

    .directive('autoSlider', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            scope: {
                value: '=homeSlider'
            },
            link: function (scope, element, attr) {
                $timeout(function() {
                    element.slick({
                        autoplay: true,
                        fade: true,
                        cssEase: 'linear',
                        dots: false,
                        infinite: true,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        nextArrow: '',
                        prevArrow: '',
                        adaptiveHeight: true,
                        adaptiveWidth: true,
                        responsive: true,
                        autoplaySpeed: 15000,
                    });
                });
            }
        }
    }])

    .directive('skrollr', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            scope: {
                value: '=skrollr'
            },
            link: function (scope, element, attr) {
                $timeout(function() {
                    skrollr.init({
                        forceHeight: false
                    });
                    setTimeout(function() {
                        skrollr.get().refresh();
                    }, 1000);
                });
            }
        }
    }])

    .directive('openMegaMenu', function() {
        return {
            restrict: 'A',
            link: function(scope, element, attr) {
                element.click(function(){
                    $('.megaMenu').slideDown( "slow", function() {
                        $('.megaMenu').show();
                    });
                });
            }
        }
    })
    .directive('closeMegaMenu', function() {
        return {
            restrict: 'A',
            link: function(scope, element, attr) {
                element.click(function(){
                    $(this).parent().slideUp( "slow", function() {
                        $(this).hide();
                    });
                });
            }
        }
    })
    .directive('spotDetailPics', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                $timeout(function() {
                    element.slick({
                        dots: false,
                        infinite: true,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        nextArrow: '<div class="slickArr slick-next">NE<br/>XT</div>',
                        prevArrow: '<div class="slickArr slick-prev">PR<br/>EV</div>',
                        adaptiveHeight: true,
                        adaptiveWidth: true,
                        responsive: true,
                    });
                });
            }
        }
    }])
    .directive('spotList', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                $timeout(function() {
                    element.slick({
                        dots: false,
                        infinite: true,
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        nextArrow: '<div class="slickArr slick-next">NE<br/>XT</div>',
                        prevArrow: '<div class="slickArr slick-prev">PR<br/>EV</div>',
                        variableWidth: true,
                        adaptiveHeight: false,
                    });
                });
            }
        }
    }])

    .directive('casesGallery', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            scope: {
                value: '=casesGallery'
            },
            link: function (scope, element, attr) {
                $timeout(function() {
                if(element.children().length > 3) {
                    element.removeClass('hasNoGallery');
                    element.slick({
                        dots: true,
                        infinite: true,
                        slidesToShow: 3,
                        slidesToScroll: 1,
                        nextArrow: '',
                        prevArrow: '',
                        variableWidth: true,
                        adaptiveHeight: false,
                        autoWidth: true,
                        centerMode: true,
                        responsive: true
                    });
                }
                });
            }
        }
    }])
    .directive('scrollie', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            scope: {
                value: '=scrollie'
            },
            link: function (scope, element, attr) {
                $timeout(function() {
                    element.scrollie({
                        scrollOffset : -(window.innerHeight / 2) + 120,
                        scrollingInView : function(elem) {

                            var bgColor = scope.value;

                            $('.mainContent').css('background-color', bgColor);

                        }
                    });
                });
            }
        }
    }])

    .directive('aComplete', function () {
        return {
            restrict: 'A',
            scope: {
                list: '=aComplete',
                action: '=aCompleteAction'
            },
            link: function(scope, element, attrs) {
                var list = element.siblings('.autocomplete');
                function setText(elems) {
                    var focus = elems.filter('.focus');
                    if(focus.length) element.val(focus.find('.ac-text').text());
                    else element.val('');
                }
                function setScroll(elems, top) {
                    var focus = elems.filter('.focus');
                    if(focus.length) {
                        focus[0].scrollIntoView(top);
                    }
                }
                var keys = [40, 38, 27, 13];
                element.on('keydown', function(e) {
                    if(keys.indexOf(e.keyCode) > -1) {
                        var elems = list.find('.ac-element');
                        var focus = elems.filter('.focus');
                        e.preventDefault();
                        switch(e.keyCode) {
                            case 40:
                                if(focus.length) focus.removeClass('focus').next().addClass('focus');
                                else elems.first().addClass('focus');
                                setText(elems);
                                setScroll(elems, false);
                            break;
                            case 38:
                                if(focus.length) focus.removeClass('focus').prev().addClass('focus');
                                else elems.last().addClass('focus');
                                setText(elems);
                                setScroll(elems, true);
                            break;
                            case 27:
                                scope.$apply(function() {
                                    scope.list = [];
                                });
                            break;
                            case 13:
                                if(scope.action) {
                                    var val = null;
                                    if(focus.length)
                                        val = scope.list[elems.index(focus)];
                                    scope.$apply(function() {
                                        scope.action(val, element.val());
                                    });
                                }
                            break;
                        }
                    }
                });
                element.on('focus', function() {
                    if(scope.list.length) list.show();
                });
                element.on('blur', function() {
                    list.hide();
                });
                list.on('mousedown', '.ac-element', function(e) {
                    if(scope.action) {
                        var elem = this;
                        scope.$apply(function() {
                            scope.action(scope.list[list.find('.ac-element').index(elem)]);
                        });
                    }
                });
                scope.$watchCollection('list', function(newValue, oldValue) {
                    if(element.is(':focus')) {
                        if(newValue.length) list.show();
                        else list.hide();
                    }
                });
            }
        }
    })

    .directive('fileModel', function () {
        return {
            restrict: 'A',
            scope: {
                model: '=fileModel'
            },
            link: function(scope, element, attrs) {
                scope.$watch('model', function(newValue, oldValue) {
                    if(!newValue) element[0].value = '';
                });
                element.bind('change', function(){
                    scope.$apply(function(){
                        scope.model = element[0].files[0];
                    });
                });
            }
        };
    })

    .directive('validFile',function(){
      return {
        require:'ngModel',
        link:function(scope,el,attrs,ngModel){
          //change event is fired when file is selected
          el.bind('change',function(){
            scope.$apply(function(){
              ngModel.$setViewValue(el.val());
              ngModel.$render();
            });
          });
        }
      }
    })

    .directive('imageonload', function() {
        return {
            restrict: 'A',
            scope: {
                imageonload: '='
            },
            link: function(scope, element) {
                element.on('load', function() {
                    if(typeof scope.imageonload == 'function')
                        scope.$apply(scope.imageonload(element));
                    else
                        window[scope.imageonload](element);
                });
                element.on('error', function() {
                    if(typeof scope.imageonload == 'function')
                        scope.$apply(scope.imageonload(element, true));
                    else
                        window[scope.imageonload](element, true);
                });
            }
        };
    });

function setAllInputsDirty(scope) {
  angular.forEach(scope, function(value, key) {
    // We skip non-form and non-inputs
    if (!value || value.$dirty === undefined) {
      return;
    }
    // Recursively applying same method on all forms included in the form
    if (value.$addControl) {
      return setAllInputsDirty(value);
    }
    // Setting inputs to $dirty, but re-applying its content in itself
    if (value.$setViewValue) {
      return value.$setDirty();
    }
  });
}

function checkForm(form) {
    if(form.$invalid) {
        setAllInputsDirty(form);
        return false;
    } else return true;
}

function showImage(element, error) {
    if(error) return;
    element.addClass('img-loaded');
}

function isPlainObject(obj) {
    return Object.prototype.toString.call(obj) === "[object Object]";
}

function scrollToTop() {
    document.body.scrollTop = 0;
}

function utf8_to_b64(str) {
    return window.btoa(unescape(encodeURIComponent(str)));
}

function b64_to_utf8(str) {
    return decodeURIComponent(escape(window.atob(str)));
}
